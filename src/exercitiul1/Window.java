package exercitiul1;


import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.FileWriter;
import java.io.IOException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window extends JFrame {
    // fereastra grafica
    JButton buton1;
    JTextField casuta1, casuta2;

    Window() {
        setTitle("Nu vriau examen");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 250);
        setVisible(true);
    }

    //initializare fereastra si initializare componente + dimensiuni
    public void init() {
        this.setLayout(null);
        int width = 80;
        int height = 20;


        casuta1 = new JTextField();
        casuta1.setBounds(70, 50, width, height);

        casuta2 = new JTextField();
        casuta2.setBounds(70, 100, width, height);

        buton1 = new JButton("Scriere");
        buton1.setBounds(10, 150, width, height);


        add(casuta1);
        add(casuta2);
        add(buton1);
        buton1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {


                try {
                    String nume = casuta1.getText();
                    FileWriter filename = new FileWriter(nume);
                    String deScris = casuta2.getText();
                    filename.write(deScris);
                    filename.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


            }
        });

    }

    public static void main(String[] args) {
        Window a = new Window();
        System.out.println("alt test");
    }


}
